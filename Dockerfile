FROM dockcross/linux-armv7a:latest

ENV DEBIAN_FRONTEND="noninteractive"

ARG uid
ARG branch=master
ENV branch=$branch

# Add libretro dependencies
RUN apt-get update --yes && apt-get install --no-install-recommends --yes \
  bsdmainutils \
&& apt-get clean --yes

RUN useradd -d /developer -m developer && \
    chown -R developer:developer /developer && \
    echo "developer:developer" | chpasswd && \
    adduser developer sudo

ENV HOME=/developer

USER root
WORKDIR /developer
VOLUME /developer

CMD /bin/bash
